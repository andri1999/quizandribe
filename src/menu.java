import java.util.Scanner;

public class menu {
    public static VoucherReedem voucherReedem = new VoucherReedem();

    public void mainMenu() throws Exception {
        try {

            System.out.println("1. Menentukan Point Dengan Jumlah Terbesar");
            System.out.println("2. Menghitung Sisa Poin Setelah di Tukar Dengan Point Terbesar (Jika Poin Adalah 1000");
            System.out.println("0. Exit");
            Scanner input = new Scanner(System.in);
            System.out.println("Masukkan Pilihan Angka Yang Diinginkan :");
            if (!input.hasNextInt()) {
                System.out.println("Masukkan Angka");
                this.promptEnterKey();
                this.mainMenu();
            }
            int pilihan = input.nextInt();
            switch (pilihan) {
                case 1 -> {
                    int point = this.point();
                    VoucherReedem voucher = voucherReedem.convertVoucher(point);
                    System.out.println("=======================================");
                    System.out.println("Tes 1 : " + voucher.getMessage());
                    System.out.println("=======================================");
                    this.promptEnterKey();
                }
                case 2 -> {
                    int point = this.point();
                    VoucherReedem Test2Input = voucherReedem.convertVoucher(point);
                    VoucherReedem Test2 = voucherReedem.convertVoucher(1000);
                    System.out.println("=============================================");
                    System.out.println("Test 2 : " + "Remaining point : " + Test2Input.getRemainingPoint());
                    System.out.println("Test 2 : If point = 1000, " + "remaining point : " + Test2.getRemainingPoint());
                    System.out.println("==============================================");
                    this.promptEnterKey();
                }
//                case 3 -> {
//                    int point = this.point();
//                    VoucherReedem voucherTest3 = TestThree(point);
//                    System.out.println(line);
//                    System.out.println(voucherTest3.getMessage());
//                    System.out.println(line);
//                    this.promptEnterKey();
//                }
                case 0 -> {
                    System.out.println("=======================================");
                    System.out.println("Finish App");
                    System.out.println("========================================");
                    System.exit(0);
                }
                default -> {
                    System.out.println("Pilihan Tidak Tersedia !");
                    System.out.println("Tekan Enter Untuk Kembali Ke MeEnu");
                    this.promptEnterKey();
                }
            }
            this.mainMenu();
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public void promptEnterKey(){
        System.out.println("Press \"ENTER\" to continue...");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
    }
    public int point() throws Exception {
        Scanner input = new Scanner(System.in);
        System.out.println("Masukkan Jumlah Poin :");
        if (!input.hasNextInt()) {
            System.out.println("Hanya Inputan 1-2");
            this.promptEnterKey();
            this.mainMenu();
        }
        int inputPoint = input.nextInt();
        if (inputPoint <= 0) {
            System.out.println("Input must > 0");
            this.promptEnterKey();
            this.mainMenu();
        }
        return inputPoint;
    }
}
