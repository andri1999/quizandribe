class VoucherReedem {
    private int SisaPoint;
    private String voucherRedeem;
    private String message;
    public VoucherReedem(){
    }
    public VoucherReedem(int SisaPoint, String message, String voucherRedeem){
        this.SisaPoint =SisaPoint;
        this.message = message;
        this.voucherRedeem = voucherRedeem;
    }

    public String getRedeemVoucher() {
        return voucherRedeem;
    }

    public void setRedeemVoucher(String redeemVoucher) {
        this.voucherRedeem = redeemVoucher;
    }

    public int getRemainingPoint() {
        return SisaPoint;
    }

    public void setRemainingPoint(int SisaPoint) {
        this.SisaPoint = SisaPoint;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public VoucherReedem convertVoucher(int point){
        if(point >= 800){
            message = "Poin Anda : "+point+" Akan Mendapatkan Voucher Senilai Rp. 100.000";
            SisaPoint = point - 800;
            voucherRedeem = "Rp. 100.000";
        } else if (point >= 400) {
            message = "Poin Anda : "+point+"Akan Mendapatkan Voucher Senilai Rp. 50.000";
            SisaPoint = point - 400;
            voucherRedeem = "Rp. 50.000";
        } else if (point >= 200) {
            message = "Poin Anda : "+point+"Akan Mendapatkan Voucher Senilai Rp. 25.000";
            SisaPoint = point - 200;
            voucherRedeem = "Rp. 25.000";
        } else if (point >= 100) {
            message = "Point Anda : "+point+"Akan Mendapatkan Voucher Senilai Rp. 10.000";
            SisaPoint = point - 100;
            voucherRedeem = "Rp. 10.000";
        } else {
            message = "Poin Anda : "+point+"Tidak Ada Poin Untuk Di Tukarkan !!!";
            SisaPoint = point;
            voucherRedeem = "";
        }

        return new VoucherReedem(SisaPoint,message,voucherRedeem);
    }
}